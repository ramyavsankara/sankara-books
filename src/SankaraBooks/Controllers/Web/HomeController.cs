﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

namespace SankaraBooks.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "SanakaraBooks Online Page";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Contact Page for SankaraBooks";

            return View();
        }

        public IActionResult Books()
        {
            return View();
        }
        public IActionResult Author()
        {
            return View();
        }
        public IActionResult Error()
        {
            return View();
        }
    }
}
